from __future__ import unicode_literals

import urllib
import base64

from django.db import models
from django.db.models import Max
from django.utils.translation import ugettext as _
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType

from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA


class Merchant(models.Model):

    MERCHANT_TYPES = (
        ('gpwebpay', 'GP webpay'),
        ('csob_platebni_brana', 'CSOB platebni brana'),
    )

    name = models.CharField(_('Name (internal)'), max_length=200, null=False)
    number = models.CharField(_('Merchant ID'), unique=True, null=False, max_length=100)
    gate_url = models.CharField(_('Gate url'), max_length=255, null=False)
    merchant_private_key = models.TextField(_('Merchant private key'), null=False)
    gate_public_key = models.TextField(_('Gate (GP webpay) public key'), null=False)
    type = models.CharField(_('Type'), null=False, blank=False, max_length=50, choices=MERCHANT_TYPES,
                            default='gpwebpay')

    def __unicode__(self):
        return self.name

    def get_new_order_number(self):
        max_number = Order.objects.filter(merchant=self).aggregate(Max('number'))['number__max']
        return max_number + 1 if max_number else 1

    def create_new_order(self, amount, currency, content_object=None, description=None, merchant_data=None,
                         merchant_order_number=0):
        order = Order(
            merchant=self,
            number=self.get_new_order_number(),
            amount=amount,
            currency=currency,
            description=description,
            merchant_data=merchant_data,
            merchant_order_number=merchant_order_number,
        )
        if content_object:
            order.content_object = content_object
        order.save()
        return order


class Order(models.Model):

    CURRENCY_CZK = 203
    CURRENCY_EUR = 978
    CURRENCY_GBP = 826
    CURRENCY_HUF = 348
    CURRENCY_PLN = 985
    CURRENCY_RUB = 643
    CURRENCY_USD = 840

    CURRENCY_CHOICES = (
        (CURRENCY_CZK, 'CZK'),
        (CURRENCY_USD, 'USD'),
        (CURRENCY_EUR, 'EUR'),
        (CURRENCY_GBP, 'GBP'),
        (CURRENCY_HUF, 'HUF'),
        (CURRENCY_PLN, 'PLN'),
        (CURRENCY_RUB, 'RUB'),
    )

    PARAMS_TO_GATE_ORDER = ('MERCHANTNUMBER', 'OPERATION', 'ORDERNUMBER', 'AMOUNT', 'CURRENCY', 'DEPOSITFLAG',
                            'MERORDERNUM', 'URL', 'DESCRIPTION', 'MD')
    PARAMS_FROM_GATE_ORDER_GPWEBPAY = ('OPERATION', 'ORDERNUMBER', 'MERORDERNUM', 'MD', 'PRCODE', 'SRCODE',
                                       'RESULTTEXT')
    PARAMS_FROM_GATE_ORDER_CSOB = ('OPERATION', 'ORDERNUMBER', 'MERORDERNUM', 'MD', 'PRCODE', 'SRCODE')

    content_type = models.ForeignKey(ContentType, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    merchant = models.ForeignKey('Merchant', null=False, verbose_name=_('Merchant'))
    number = models.BigIntegerField(_('Order number'), null=False)
    merchant_order_number = models.BigIntegerField(_('Merchant order number'), null=False, blank=False, default=0)
    amount = models.BigIntegerField(_('Amount'), null=False)
    currency = models.PositiveIntegerField(_('Currency'), null=False, choices=CURRENCY_CHOICES)

    # Only ASCII 0x20 - 0x7E
    description = models.TextField(_('Description'), null=True, blank=True)

    # MD, only ASCII 0x20 - 0x7E
    merchant_data = models.TextField(_('Merchant data'), null=True, blank=True)

    prcode = models.IntegerField(_('PRCODE'), null=True, blank=True)
    srcode = models.IntegerField(_('SRCODE'), null=True, blank=True)
    result_text = models.TextField(_('Result text'), null=True, blank=True)

    created = models.DateTimeField(_('Created'), null=False, auto_now_add=True)
    updated = models.DateTimeField(_('Updated'), null=False, auto_now=True)

    def __unicode__(self):
        return _('Order #%s') % self.pk

    def get_params_from_gate_order(self):
        if self.merchant.type == 'csob_platebni_brana':
            return self.PARAMS_FROM_GATE_ORDER_CSOB
        else:
            return self.PARAMS_FROM_GATE_ORDER_GPWEBPAY

    def get_gate_url_params_as_string(self, back_url, deposit_flag=True):
        params = self.get_gate_url_params(back_url, deposit_flag)
        params['DIGEST'] = self.get_digest_from_params(params)
        encoded_params = []
        for param in self.PARAMS_TO_GATE_ORDER + ('DIGEST',):
            if not params.get(param, None) is None:
                encoded_params.append(urllib.urlencode({param: params[param]}))
            else:
                encoded_params.append('%s=' % param)
        return '&'.join(encoded_params)

    def get_gate_url_params(self, back_url, deposit_flag=True):
        # Keys of Dictionary correspond to documentation of GPwebpay
        return {
            'MERCHANTNUMBER': self.merchant.number,
            'OPERATION': 'CREATE_ORDER',
            'ORDERNUMBER': self.number,
            'AMOUNT': self.amount,
            'CURRENCY': self.currency,
            'DEPOSITFLAG': 1 if deposit_flag else 0,
            'MERORDERNUM': self.merchant_order_number,
            'URL': back_url,
            'DESCRIPTION': self.description,
            'MD': self.merchant_data
        }

    def get_digest_from_params(self, params):
        plain_text = self.params_to_plain_text(params)
        private_key = RSA.importKey(self.merchant.merchant_private_key)
        if not private_key.has_private():
            raise Exception('Invalid private key!')

        h = SHA.new(plain_text)
        signer = PKCS1_v1_5.new(private_key)
        signature = signer.sign(h)
        return base64.b64encode(signature)

    def params_to_plain_text(self, params):
        return '|'.join([str(params.get(param_name)) if not params.get(param_name) is None else '' for param_name in
                         self.PARAMS_TO_GATE_ORDER])

    def verify_params_to_plain_text(self, params):
        return '|'.join([str(params.get(param_name)) if not params.get(param_name, None) is None else ''
                         for param_name in self.get_params_from_gate_order()])

    def verify_digest(self, params, digest, digest_1=False):
        plain_text = self.verify_params_to_plain_text(params)
        if digest_1:
            plain_text += '|%s' % self.merchant.number
        h = SHA.new(plain_text)
        public_key = RSA.importKey(self.merchant.gate_public_key.encode('ascii'))
        signer = PKCS1_v1_5.new(public_key)
        return signer.verify(h, digest)

    def verify_order(self, request):
        data = request.GET
        params = dict((param, data.get(param, None)) for param in self.get_params_from_gate_order())

        if not self.verify_digest(params, base64.b64decode(data.get('DIGEST', ''))):
            return False

        if not self.verify_digest(params, base64.b64decode(data.get('DIGEST1', '')), digest_1=True):
            return False

        if not str(self.number) == params['ORDERNUMBER']:
            return False

        if not str(self.merchant_order_number) == params['MERORDERNUM']:
            return False

        self.prcode = int(params['PRCODE'])
        self.srcode = int(params['SRCODE'])
        self.result_text = params.get('RESULTTEXT')
        self.save()

        return True

    def is_paid(self):
        return self.prcode == 0 and self.srcode == 0

    class Meta:
        unique_together = ('merchant', 'number')
