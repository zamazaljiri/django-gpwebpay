from __future__ import unicode_literals

from django.contrib import admin


class MerchantAdmin(admin.ModelAdmin):
    list_display = ('name', 'number')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('pk', 'number', 'merchant', 'merchant_order_number', 'amount', 'currency', 'prcode', 'srcode',
                    'result_text', 'created', 'updated')
    search_fields = ('id', 'number', 'merchant_order_number', 'amount')
    list_filter = ('merchant', 'currency', 'prcode', 'srcode', 'created', 'updated')
    readonly_fields = ('created', 'updated')

