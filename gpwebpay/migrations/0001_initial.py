# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Merchant'
        db.create_table(u'gpwebpay_merchant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('number', self.gf('django.db.models.fields.IntegerField')(unique=True)),
            ('gate_url', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('merchant_private_key', self.gf('django.db.models.fields.TextField')()),
            ('gate_public_key', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'gpwebpay', ['Merchant'])

        # Adding model 'Order'
        db.create_table(u'gpwebpay_order', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'], null=True, blank=True)),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('merchant', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gpwebpay.Merchant'])),
            ('number', self.gf('django.db.models.fields.IntegerField')()),
            ('merchant_order_number', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('amount', self.gf('django.db.models.fields.IntegerField')()),
            ('currency', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('merchant_data', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('prcode', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('srcode', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('result_text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'gpwebpay', ['Order'])

        # Adding unique constraint on 'Order', fields ['merchant', 'number']
        db.create_unique(u'gpwebpay_order', ['merchant_id', 'number'])


    def backwards(self, orm):
        # Removing unique constraint on 'Order', fields ['merchant', 'number']
        db.delete_unique(u'gpwebpay_order', ['merchant_id', 'number'])

        # Deleting model 'Merchant'
        db.delete_table(u'gpwebpay_merchant')

        # Deleting model 'Order'
        db.delete_table(u'gpwebpay_order')


    models = {
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'gpwebpay.merchant': {
            'Meta': {'object_name': 'Merchant'},
            'gate_public_key': ('django.db.models.fields.TextField', [], {}),
            'gate_url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'merchant_private_key': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'number': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        u'gpwebpay.order': {
            'Meta': {'unique_together': "((u'merchant', u'number'),)", 'object_name': 'Order'},
            'amount': ('django.db.models.fields.IntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'merchant': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gpwebpay.Merchant']"}),
            'merchant_data': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'merchant_order_number': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'prcode': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'result_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'srcode': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['gpwebpay']