# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Merchant.number'
        db.alter_column(u'gpwebpay_merchant', 'number', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100))

    def backwards(self, orm):

        # Changing field 'Merchant.number'
        db.alter_column(u'gpwebpay_merchant', 'number', self.gf('django.db.models.fields.IntegerField')(unique=True))

    models = {
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'gpwebpay.merchant': {
            'Meta': {'object_name': 'Merchant'},
            'gate_public_key': ('django.db.models.fields.TextField', [], {}),
            'gate_url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'merchant_private_key': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        u'gpwebpay.order': {
            'Meta': {'unique_together': "((u'merchant', u'number'),)", 'object_name': 'Order'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'merchant': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gpwebpay.Merchant']"}),
            'merchant_data': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'merchant_order_number': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'number': ('django.db.models.fields.BigIntegerField', [], {}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'prcode': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'result_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'srcode': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['gpwebpay']