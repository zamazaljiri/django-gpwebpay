from __future__ import unicode_literals

from django.views.generic import RedirectView
from django.utils import translation
from django.db import transaction

from gpwebpay.models import Order


class RedirectToGateView(RedirectView):
    permanent = False

    def get_merchant(self):
        raise NotImplementedError()

    def create_order(self, merchant):
        raise NotImplementedError()

    def get_lang(self):
        return translation.get_language()

    @transaction.commit_on_success
    def get_redirect_url(self, **kwargs):
        merchant = self.get_merchant()
        new_order = self.create_order(merchant)
        return '%s?%s&lang=%s' % (
            merchant.gate_url,
            new_order.get_gate_url_params_as_string(back_url=self.get_back_url(new_order)),
            self.get_lang()
        )

    def get_back_url(self, order):
        raise NotImplementedError()


class ReceiveOrderFromGateMixin(object):

    def get_order(self):
        try:
            return Order.objects.get(id=self.kwargs['pk'])
        except Order.DoesNotExist:
            return None