# README #

Aktuální verze: 0.1.2

Co je nového v 0.1.2: Podpora platební brány ČSOB (https://github.com/csob/paymentgateway/wiki) - z toho důvodu se u modelu Merchant nastavuje typ (gpwebpay nebo ČSOB). Merchant ID již není číslo, ale string (kvůli ČSOB). Změna verifikování odpovědi od brány (opět kvůli rozdílným parametrům v ČSOB).

## Úvod ##
* Jednoduchá django aplikace pro integraci platební brány gpwebpay (www.gpwebpay.cz)
* Aplikace podporuje více obchodních účtů - proto se aplikace nenastavuje v settings, ale v modelu *Merchant*
* Aplikace zatím podporuje jen přesměrování na platební bránu a příjem objednávky z platební brány (HTTP redirect)
* Aplikace neumí volání webových služeb
* Aplikace používá knihovnu [PyCrypto](https://pypi.python.org/pypi/pycrypto/2.6.1)

* TODO: Webové služby platební brány

## Instalace ##


```
#!
pip install https://bitbucket.org/zamazaljiri/django-gpwebpay/get/0.1.2.tar.gz#egg=django-gpwebpay-0.1.2
```

Settings:

```
#!python

INSTALLED_APPS = (
    ....
    'south',
    'gpwebpay'
)
```

Migrace:
```
#!
python manage.py migrate gpwebpay
```


## View - přesměrování na platební bránu ##

Stačí podědit view *RedirectToGateView* a doimplementovat metody:
* *get_merchant* - vrací model obchodníka
* *create_order* - vytváří model objednávky. Zde se určuje částka, měna, popis, apod.
* *get_back_url* - vrací adresu návratového view, které zpracuje příjem objednávky.

Ukázka:

```
#!python

class RedirectToGate(RedirectToGateView):

    def get_merchant(self):
        return Merchant.objects.get(id=self.kwargs['pk'])

    def create_order(self, merchant):
        return merchant.create_new_order(
            amount=1200,
            currency=Order.CURRENCY_EUR, # Podporované měny jsou uložené jako konstanty ve třídě Order. Kódy měn viz dokumentace GPwebpay.
            description='popis objednavky', # Musí být pouze ASCII (0x20 - 0x7E). Max 255 znaků.
            merchant_data='custom_data',  # Musí být pouze ASCII (0x20 - 0x7E). Max 255 znaků.
            merchant_order_number=8874 # Například číslo faktury - pro lepší orientaci v administraci GPwebpay
        )

    def get_back_url(self, order):
        return '%s%s' % (settings.PROJECT_URL, reverse('receive-payment', args=(order.pk,)))
```

## View - příjem objednávky ##

Pro příjem objednávky je nutné si napsat view. Je připraven pouze jednoduchý Mixin. Ukázka:


```
#!python

class ReceiveOrderFromGate(ReceiveOrderFromGateMixin, View):
    template_name = 'receive.html'

    def do_xxx(self, **kwargs):
        order = self.get_order()

        if order.is_paid():
            # Objednávka byla již zpracována a nastavena jako zaplacená. Uživatel mohl kliknout na tlačítko zpět nebo si zkopíroval URL adresu, kterou volá platební brána.
            print "Jiz bylo zaplaceno"
        else:
            if order and order.verify_order(self.request):
                # Příjem je v pořádku (podpis DIGEST a DIGEST1 souhlasí). To ještě ale neznamená, že platba byla provedena!
                if order.is_paid():
                    # Zpracování zaplacené objednávky. Například nastavení faktury, nastavení hlášky a přesměrování na košík / detail faktury, apod.
                    print "Zaplaceno"
                else:
                    # Platba se nezdařila. Například špatné číslo karty, expirace, 3D, apod. Zobrazení hlášky, kterou poslala brána.
                    print order.result_text
            else:
                # Neplatný požadavek. Chybný DIGEST, DIGEST1 nebo jiná chyba. Vrátit 403 nebo něco podobného.
                print "Neplatny pozadavek"
```

## Nastavení klíčů ##

V modelu *Merchant* se nastavuje soukromý klíč obchodníka a veřejný klíč platební brány (veřejný klíč platební brány může být u každého obchodníka teoreticky jiný, proto se nastavuje u každého modelu zvlášť).

GPwebpay dodává program pro vygenerování klíčů. Vygenerovaný soubor .pfx je ale nutné převést do správného formátu.


```
#!
openssl pkcs12 -in aaa.pfx -nocerts -out private_key.pem
```

A ještě musíme privátní klíč zbavit hesla:
```
#!
openssl rsa -in private_key.pem -out private_key_without_pass.pem
```

Jen pro úplnost - postup exportu veřejného klíče z .pfx:

```
#!
openssl pkcs12 -in aaa.pfx -clcerts -nokeys -out public_key.pem

```

Nebo je také možné veřejný klíč vygenerovat z privátního:
```
#!
openssl rsa -in private_key.pem -pubout > key.pub
```

Převod produkčního certifikátu MUZO do public key (rsa):
```
#!
openssl x509 -inform der -in muzo.signing_prod.cer -noout -pubkey > pubkey.pub
```

Privátní klíč obchodníka musí být v modelu Merchant uložen v následujícím tvaru:

```
#!

-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAgEIgM6RFTxwAtaafcDlrhB0Lv1v6j0rYFsEIaTByG6hEsE1R
fUEjHF0I45mbbRhAEk1I8eTWYYJXZ/8sBrJJaHLVUZfIsFDTO5FZDhhukdtfW6oe
/NqBc19sVtvGDIKSm5OoW5SbHpvVaiM3VOlUG/wi1oQ551yRZKxOw0KeFj1SGtW5
...
...
...
-----END RSA PRIVATE KEY-----
```

Veřejný klíč platební brány musí být uložen v následujícím tvaru:

```
#!

-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAukooJBqkgau4pjhxkDC3
5hDl0SHOn3BonEU1ldz/l6886nJhypHNkLSHgBJRvhbLz/QdOD3mZShrupcxNRKT
...
...
...
-----END PUBLIC KEY-----
```

Jiné typy formátování / klíčů **nemusí** (a ve většině případů ani **nejsou**) **podporovány**.

**Pokud klíče obsahují následující řádky, aplikace se nemusí chovat správně:**


```
#!

Bag Attributes
    friendlyName: PayMuzo
    localKeyID: .. ... .. .. .. ..
Key Attributes: <No Attributes>


-----BEGIN ENCRYPTED PRIVATE KEY-----

Bag Attributes
    friendlyName: PayMuzo
    localKeyID: .. .. .. .. ..
subject=...
issuer=....


-----BEGIN CERTIFICATE-----
```

## Číslování objednávek ##

GPwebpay požaduje, aby každý požadavek na platební bránu měl unikátní číslo objednávky. To znamená, že ještě před přesměrováním na platební bránu je nutné vytvořit model *Order* s unikátním číslem objednávky (atribut **number**). Každý obchodní účet má vlastní číslování objednávek.

Pokud u obchodníka není evidována žádná objednávka, při prvním přesměrování na platební bránu se vytvoří objednávka s číslem 1.

Pokud jsou u obchodníka evidovány objednávky, vezme se nejvyšší číslo objednávky, přičte se číslo 1 => to je nové číslo objednávky.

### Problémy s číslováním objednávek ###

Pokud budete přecházet ze starého systému na nový (například aplikace napsaná v PHP -> Django app) a budete používat stejný obchodní účet GPwebpay, mohou nastat problémy. 

Předpokládejme, že v obchodním účtu na GPwebpay máte objednávky s čísly 1000 - 5417. Uděláte novou Django aplikaci, vytvoříte nový obchodní účet a tabulka objednávek je prázdná.

* První přesměrování na platební bránu = vytvoření objednávky s číslem 1
* ...
* Desáté přesměrování na platební bránu = vytvoření objednávky s číslem 10
* 999 přesměrování na platební bránu = vytvoření objednávky s číslem 999
* 1000 přesměrování na platební bránu = vytvoření objednávky s číslem 1000 = platební vrátí chybu, toto číslo objednávky již bylo použito!
* Všechny další požadavky na přesměrování vrátí chybu - až do čísla 5417.

Správný postup: Uděláte novou Django aplikaci, vytvoříte nový obchodní účet a ručně v administraci vytvoříte objednávku s číslem 5417 (nebo i vyšším).

* První přesměrování na platební bránu = vytvoření objednávky s číslem 5418 = platební brána nic nenamítá
* Druhé přesměrování na platební bránu = vytvoření objednávky s číslem 5419 = platební brána nic nenamítá

Další problém by mohl nastat, pokud byste stejný obchodní účet chtěli používat ve více aplikacích (projektech) najednou. Každý projekt by pak musel používat vlastní číslování (například unikátní prefix čísel objednávek)

## Testovací vs. produkční prostředí ##

Přechod z testovacího prostředí do produkčního je jednoduchý. Stačí v modelu Merchant změnit následující atributy:

* gate_url
* merchant_private_key
* gate_public_key

A (nejen) z důvodu konfliktů číslování objednávek odstranit všechny objednávky.

Jako lepší postup se mi zdá udělat dva modely *Merchant* a pojmenovat je jako produkční a testovací. Každý z nich bude mít vlastní objednávky. Pak už jen někde stačí nastavit, který model Merchant se má aktuálně používat (například v settings).